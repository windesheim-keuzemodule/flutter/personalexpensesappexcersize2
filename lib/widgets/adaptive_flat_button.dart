import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class AdaptiveFlatButton extends StatelessWidget {
  final String text;
  final Function handler;

  AdaptiveFlatButton(this.text, this.handler);

  @override
  Widget build(BuildContext context) {
    return Platform.isIOS
        ? CupertinoButton(
            child: TextButton(
              child: Text(
                this.text,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all(Colors.red),
              ),
            ),
            onPressed: this.handler,
          )
        : Container(
            margin: EdgeInsets.only(left: 10),
            child: TextButton(
              child: Text(
                this.text,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all(Colors.red),
              ),
              onPressed: this.handler,
            ),
          );
  }
}
